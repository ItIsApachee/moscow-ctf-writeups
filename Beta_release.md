# Beta release

![img](https://i.imgur.com/cyiCz34.png)

## Решение:

Изучив функцианал сервиса, воспользуемся утилитой для поиска скрытых директорий [dirb](https://tools.kali.org/web-applications/dirb).

![img](https://i.imgur.com/fyguXBF.png)

Видим окрытую директорию .git ([подробнее о .git](https://habr.com/ru/post/143079/)).
Директория .git является стандартной для репозитория git и содержит информацию обо всех изменеиях в проэкте.
Так к как файлов в .git довольно много, воспользуемся [утилитой](https://github.com/internetwache/GitTools), автоматизирующей скачивание и дешифрование файлов.

Для начал загрузм содержимое .git на локальный компьютер.
![img](https://i.imgur.com/HOJ6XZW.png)

Затем запустим небольшой bash-скрипт для извлечения коммитов и их содержимого из сломанного репозитория.
![img](https://i.imgur.com/ZFFPsQd.png)

Теперь, зная формат флага, мы можем найти флаг в исходном коде web-приложения.
![img](https://i.imgur.com/YnyKcQN.png)
