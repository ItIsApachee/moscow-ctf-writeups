# CD-key

![img](https://i.imgur.com/sRdvWtp.png)

## Решение:

Скачиваем файл и первым делом исполняем `strings cdkey.exe`. В выводе можем наблюдать кучу файлов python:

```
spyi_rth__tkinter
b_socket.pyd
b_tkinter.pyd
bpyexpat.pyd
bpython37.dll
%python37.dll
```

Делаем вывод о том, что программа написана на python и извлекаем исходный код с помощью найденного на гитхабе скрипта, например этого - (https://github.com/countercept/python-exe-unpacker)

Запускаем скрипт
`python python_exe_unpack.py -i cdkey.exe`

В результате папка с файлами среди которых куча мусора (языковые настройки или типо того), а также файл `keycheck`, который надо декомпилировать в скрипт питона с помощью, например, `uncompyle6`, предварительно добавив в начало файла magic number `42 0d 0d 0a 00 00 00 00 00 00 00 00 00 00 00 00` любым hex редактором.

В итоге исходный скрипт выглядит так:

```python
import sys, functools, hashlib
ALPHABET = 'ZAC2B3EF4GH5TK67P8RS9WXY'

def strtoint(s):
    return functools.reduce(lambda x, y: x * len(ALPHABET) + y, map(ALPHABET.index, s), 0)


def check_key(key):
    parts = key.split('-')
    if len(parts) != 4:
        return False
    elif not all(len(part) == 5 for part in parts):
        return False
    elif not all(c in ALPHABET for part in parts for c in part):
        return False
    else:
        expected_checksum = int.from_bytes(hashlib.blake2b('-'.join(parts[::2]).encode()).digest(), 'big')
        expected_checksum %=  len(ALPHABET) ** len(''.join(parts[1::2]))
        checksum = strtoint(''.join(parts[1::2]))
        return expected_checksum == checksum


if __name__ == '__main__':
    print(check_key(sys.argv[1]))

```

По-сути скрипт проверяет символы в ключе из переменной `ALPHABET`, формат ключа и считает контрольную сумму. Проверка происходит по частям (хэш от 1 и 3 части сравнивается с 2 и 4)

Пробуем захешировать свой тестовый ключ, для этого достаточно взять 1 и 3 части и заполнить их случайными символами `XXXXX-YYYYY`, итог - `27573412365809`

Далее надо перебрать такие 2 и 4 части, которые после применения к ним функции `strtoint` дадут `27573412365809`

Вот как это можно реализовать:

```python
import sys
from z3 import *
ALPHABET = 'ZAC2B3EF4GH5TK67P8RS9WXY'

def strtoint(str):
    out = 0
    for i in str:
        position = ALPHABET.find(i)
        out = (out * len(ALPHABET)) + position
    return out

def main():
    str  = Solver()
    ans = 27573412365809
    out = 0
    
    for i in range(10):
        c = globals()['b%d' % i] = BitVec('b%d' % i, 128)
        str.add(c >= 0, c < len(ALPHABET))
        out = (out * len(ALPHABET)) + c

    str.add(out == ans)
    
    if str.check() == sat:
        model = s.model()
        out = ''
        for i in range(10):
            c = globals()['b%d' % i]
            out += ALPHABET[model[c].as_long()]

    print(out)
    assert strtoint(out) == ans

if __name__ == "__main__":
    sys.exit(main())

```

Получаем `HH5W7ZC5T8`. Это склееные 2 и 4 часть, разбиваем их пополам и формируем ключ - `XXXXX-HH5W7-YYYYY-ZC5T8`. 

Запускаем программу, скармливаем ей ключ и получаем флаг:

`MSKCTF{OLDER_REAL_WORLD_CDKEY_CHECKS_ARE_ACTUALLY_LIKE_THIS}`
