# Dive into the cool story

![img](https://i.imgur.com/spZJ0zs.png)

## Решение:

Решив `You've got the mail`, получаем ссылку на архив coolstory.zip.
Инспектируя архив, мы видим кучу разных документов, картинок и прочего в папке `Мои документы`, однако там есть только один файл с расширением `.docx`, который при попытке открыть внутри менджера архивов GNOME открывает другой архив, в котором есть файл `flag.txt`.
![img](https://i.imgur.com/d5ANxSz.png)
Внутри файла обнаруживаем сам флаг: MSKCTF{psst_wanna_d1ve_int0_c001_st0ry}
