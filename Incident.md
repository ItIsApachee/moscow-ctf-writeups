# Incident

![img](https://i.imgur.com/z2lk9ja.png)

## Решение

Отправляемся по ссылке и видим в правом верхнем углу поисковую сторку с надписью not implemented.
![img](https://i.imgur.com/0MlT2hl.png)
Открываем BurpSuite, вводим тестовую строку и ловим пакет.
![img](https://i.imgur.com/OScKAKa.png)
Не найдя тестовой строки в запросе, убеждаемся, что функцианал поисковика не риализован.
Переходим во вкладку Response.
![img](https://i.imgur.com/Xd3mUqg.png)
Видим, что в ответе от сервера выводится отладочная информация, а точнее содержимое файла, перданного в параметре bloked-uri.
Итак у нас есть [LFI](https://en.wikipedia.org/wiki/File_inclusion_vulnerability).
Уверенным нажатием сочетания клавиш Ctrl+R отправляем запрос в Repeater.
Первым делом пробуем прочитать файл flag.txt.
![img](https://i.imgur.com/hkfQeEE.png)
Но увы, такого файла на сервере нет.
Воспользуемся методом дедукции.
![img](https://i.imgur.com/5I4qHy2.jpg)
Если существует файл not-a-flag.js, то логично предположить, что существует файл flag.js .
Проверим наши предположения на практике.
![img](https://i.imgur.com/6Td3yzn.png)
Такой файл действительно существует и содержит в себе флаг.
