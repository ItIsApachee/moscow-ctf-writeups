# You've got mail

![img](https://i.imgur.com/tPK4iAW.png)

## Решение:

Первое, что бросается в глаза при беглом осмотре файла - непечатные символы и строка в base64.

### Часть 1

Начнём со строки в base64. Присмотревшись к письму, видим, что это закодированный в base64 архив.
>filename="attach.zip"

Декодируем base64 и сохраним архив.
```sh
$ echo "UEsDBBQACQAIAFZidVBv9f5ahwAAAHkAAAAIAAAAZmxhZy50eHQ45c3zHhuqVNivOf80djnC
tmkmwiSCTELSf0iFP6kEGDd3NlChMcSk0q/kN9oCTYH1NZSli0d2DS22xNVt/wLwLEwkwxdN
CXNajXmLRN5FxD3C2/Br6aCFvpylLx6y245zkB0cd8u9ICEVoiMhjITSIsAsnarMCK8IUwZX
MLNAehKGf0nkGkFQSwcIb/X+WocAAAB5AAAAUEsBAh8AFAAJAAgAVmJ1UG/1/lqHAAAAeQAA
AAgAJAAAAAAAAAAgAAAAAAAAAGZsYWcudHh0CgAgAAAAAAABABgAmQOVuGH/1QGE2Utw6v7V
AYTZS3Dq/tUBUEsFBgAAAAABAAEAWgAAAL0AAAAAAA==" | base64 -d > mbox.zip
```
Пробуем открыть, но неудачно, архив оказался запаролен.
![img](https://i.imgur.com/1Zsng9d.png)

### Часть 2

Для определения кодировки письма содержащего непечатные символы, воспользуемся сервисом online-decoder.com .
![img](https://i.imgur.com/FmVHG62.png)
Вуаля! У нас есть пароль от архива.

Отправляемся в ос windows, так как unzip не умеет работать с паролями на кириллице. Разархивируем.
Файл flag.txt из архива содержит флаг и ссылку к следующему таску
![img](https://i.imgur.com/AqRth06.png)
